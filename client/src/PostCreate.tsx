import React, {useState} from "react"
import axios from "axios"

export default (): JSX.Element => {
  const [title, setTitle] = useState<string>('')

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    await axios.post(`${process.env.REACT_APP_POSTS_API}/posts/create`, {
      title
    })

    setTitle('')
  }

  return (
    <div>
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <label>Title</label>
          <input value={title} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setTitle(e.target.value)}
                 className="form-control"/>
        </div>
        <button className="btn btn-primary">Submit</button>
      </form>
    </div>
  )
}