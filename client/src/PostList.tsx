import React, {useEffect} from "react";
import axios from 'axios'

import CommentCreate from "./CommentCreate";
import CommentList, {IComments} from "./CommentList";

interface IPosts {
  [key: string]: {
    id: string,
    title: string,
    comments: IComments[]
  }
}

export default (): JSX.Element => {
  const [posts, setPosts] = React.useState<IPosts>({})

  const fetchPosts = async () => {
    const res = await axios.get(`${process.env.REACT_APP_QUERY_API}/posts`)

    setPosts(res.data)
  }

  useEffect(() => {
    fetchPosts()
  }, [])

  const renderedPosts: JSX.Element[] = Object.values(posts).map(post => {
    return (
      <div className="card" style={{width: '30%', marginBottom: '20px'}} key={post.id}>
        <div className="card-body">
          <h3>{post.title}</h3>
          <CommentList comments={post.comments} />
          <CommentCreate postId={post.id}/>
        </div>
      </div>
    )
  })

  return (
    <div className="d-flex flex-row flex-wrap justify-content-between">
      {renderedPosts}
    </div>
  )
}