import React from "react";
import axios from "axios"

export interface ICommentProps {
  postId: string
}

export default ({postId}: ICommentProps) => {

  const [content, setContent] = React.useState<string>('')

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    await axios.post(`${process.env.REACT_APP_COMMENTS_API}/posts/${postId}/comments`, {
      content
    })
    setContent('')
  }

  return (
    <div>
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <label>New Comment</label>
          <input value={content} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setContent(e.target.value)}
                 className="form-control"/>
        </div>
        <button className="btn btn-primary">Submit</button>
      </form>
    </div>
  )
}