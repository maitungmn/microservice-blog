import React from "react";

export interface IComments {
  id: string,
  content: string,
  status: string
}

interface ICommentListProps<T> {
  comments: T[]
}

export default ({comments}: ICommentListProps<IComments>): JSX.Element => {

  const renderedComments: JSX.Element[] = comments.map(comment => {

    let content: string = ''

    if (comment.status === 'approved') {
      content = comment.content
    }

    if (comment.status === 'pending') {
      content = 'This comment is awaiting moderation'
    }

    if (comment.status === 'rejected') {
      content = 'This comment has been rejected'
    }

    return (
      <li key={comment.id}>
        {content}
      </li>
    )
  })

  return (
    <ul>
      {renderedComments}
    </ul>
  )
}