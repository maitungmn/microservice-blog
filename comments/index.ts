import express, {Request, Response} from 'express'
import bodyParser from 'body-parser'
import {randomBytes} from 'crypto'
import cors from 'cors'
import axios from "axios"
import * as dotenv from "dotenv";

dotenv.config()

const port = 4001
const app = express()
app.use(bodyParser.json())
app.use(cors())

interface IComments {
  id: string,
  content: string,
  status: string,
}

interface ICommentsByPostId<T> {
  [key: string]: T[]
}

const commentsByPostId: ICommentsByPostId<IComments> = {}

app.get('/posts/:id/comments', (req: Request, res: Response) => {
  res.send(commentsByPostId[req.params.id] || [])
})

app.post('/posts/:id/comments', async (req: Request, res: Response) => {
  const commentId = randomBytes(4).toString('hex')
  const {content} = req.body

  const comments = commentsByPostId[req.params.id] || []
  comments.push({id: commentId, content, status: 'pending'})
  commentsByPostId[req.params.id] = comments

  await axios.post(`${process.env.EVENT_BUS_API}/events`, {
    type: 'CommentCreated',
    data: {
      id: commentId,
      content,
      postId: req.params.id,
      status: 'pending'
    }
  })

  res.status(201).send(comments)
})

app.post('/events', async (req: Request, res: Response) => {
  console.log('Event Received: ', req.body.type)

  const {type, data} = req.body

  if (type === 'CommentModerated') {
    const {postId, id, status, content} = data
    const comments = commentsByPostId[postId]
    const comment = comments.find(comment => {
      return comment.id === id
    })
    if (comment && comment.status) {
      comment.status = status
    }
    await axios.post(`${process.env.EVENT_BUS_API}/events`, {
      type: 'CommentUpdated',
      data: {
        id,
        status,
        postId,
        content,
      }
    })
  }

  res.send({})
})

app.listen(port, () => {
  console.log(`Listening on 4001`)
})