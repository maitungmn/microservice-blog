import express, {Response, Request} from "express"
import bodyParser from "body-parser"
import axios from "axios"
import * as dotenv from "dotenv";

dotenv.config()

const port = 4005

const app = express()
app.use(bodyParser.json())

interface IEvent {
  [key: string]: any
}

const events: IEvent[] = []

app.post('/events', async (req: Request, res: Response) => {
  try {
    const event = req.body

    events.push(event)

    await axios.post(`${process.env.POSTS_API}/events`, event)
    await axios.post(`${process.env.COMMENTS_API}/events`, event)
    await axios.post(`${process.env.QUERY_API}/events`, event)
    await axios.post(`${process.env.MODERATION_API}/events`, event)
    // await axios.post(`${process.env.COMMENTS_API2}/events`, event)
    // await axios.post(`${process.env.COMMENTS2_API2}/events`, event)

    res.send({status: 'OK'})
  } catch (e) {
    console.log(e)
  }

})

app.get('/events', (req: Request, res: Response) => {
  res.send(events)
})

app.listen(port, () => {
  console.log(`Listening on ${port}`)
})



