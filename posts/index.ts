import express, {Request, Response} from 'express'
import bodyParser from 'body-parser'
import {randomBytes} from 'crypto'
import cors from 'cors'
import axios from "axios"
import * as dotenv from "dotenv";

dotenv.config()

const port = 4000
const app = express()
app.use(bodyParser.json())
app.use(cors())

interface IPosts {
  [key: string]: {
    id: string,
    title: string,
  }
}

const posts: IPosts = {}

app.get('/posts', (req: Request, res: Response) => {
  res.send(posts)
})

app.post('/posts/create', async (req: Request, res: Response) => {
  const id = randomBytes(4).toString('hex')
  const {title} = req.body

  posts[id] = {
    id, title
  }

  await axios.post(`${process.env.EVENT_BUS_API}/events`, {
    type: 'PostCreated',
    data: {
      id, title
    }
  })

  res.status(201).send(posts[id])
})

app.post('/events', (req: Request, res: Response) => {
  console.log('Received Event', req.body.type)
  res.send({})
})

app.listen(port, () => {
  console.log('EVENT_BUS', process.env.EVENT_BUS_API)
  console.log('Listening on port ' + port)
})