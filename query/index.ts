import express, {Response, Request} from "express"
import bodyParser from "body-parser"
import cors from "cors"
import axios from "axios"
import * as dotenv from "dotenv";

dotenv.config()

const port = 4002

const app = express()
app.use(bodyParser.json())
app.use(cors())

interface IComments {
  id: string,
  content: string,
  status: string,
}

interface IPosts {
  [key: string]: {
    id: string,
    title: string,
    comments: IComments[],
  }
}

interface IResponseData {
  [key: string]: string
}

const posts: IPosts = {}

const handleEvent = (type: string, data: IResponseData) => {
  if (type === 'PostCreated') {
    const {id, title} = data
    posts[id] = {id, title, comments: []}
  }

  if (type === 'CommentCreated') {
    const {id, content, postId, status} = data
    const post = posts[postId]
    post.comments.push({id, content, status})
  }

  if (type === 'CommentUpdated') {
    const {id, content, postId, status} = data

    const post = posts[postId]
    const comment = post.comments.find(comment => {
      return comment.id === id
    })

    if (comment) {
      comment.status = status
      comment.content = content
    }
  }
}

app.get('/posts', (req: Request, res: Response) => {
  res.send(posts)
})

app.post('/events', (req: Request, res: Response) => {
  const {type, data} = req.body

  handleEvent(type, data)

  res.send({})
})

app.listen(port, async () => {
  console.log(`Listening on port ${port}`)

  try {
    const res = await axios.get(`${process.env.EVENT_BUS_API}/events`)
    for (let event of res.data) {
      console.log('Processing event: ', event.type)

      handleEvent(event.type, event.data)
    }
  } catch (e) {
    console.log(e)
  }
})