import express, {Response, Request} from "express"
import bodyParser from "body-parser"
import axios from "axios"
import * as dotenv from "dotenv";

dotenv.config()

const app = express()
app.use(bodyParser.json())

const port = 4003

app.post('/events', async (req: Request, res: Response) => {
  try {
    const {type, data} = req.body
    if (type === 'CommentCreated') {
      const status = data.content.includes('orange') ? 'rejected' : 'approved'

      await axios.post(`${process.env.EVENT_BUS_API}/events`, {
        type: 'CommentModerated',
        data: {
          id: data.id,
          postId: data.postId,
          status,
          content: data.content,
        }
      })
    }
    res.send({})
  } catch (e) {
    console.log(e)
  }

})

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})